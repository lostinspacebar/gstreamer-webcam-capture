CC=g++
CFLAGS=`pkg-config --cflags gstreamer-0.10` `pkg-config --cflags glib-2.0` `pkg-config --cflags opencv`
LDFLAGS=`pkg-config --libs glib-2.0` `pkg-config --libs gstreamer-0.10` `pkg-config --libs opencv`

all: main.cpp
	$(CC) $(CFLAGS) main.cpp $(LDFLAGS) -o gst
