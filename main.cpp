#include <gst/gst.h>
#include <sys/time.h>
#include "opencv2/opencv.hpp"

using namespace cv;

timeval t;
double last = 0;
int fps = 0;

void sink_callback(GstElement *sink)
{
	GstBuffer *buffer;
	g_signal_emit_by_name(sink, "pull-buffer", &buffer);
	
	Mat image(Size(320, 240), CV_8UC3, GST_BUFFER_DATA(buffer));
	Mat gray;
	cvtColor(image, gray, CV_RGB2GRAY);
	Mat sobel;
	Sobel(gray, sobel, CV_8U, 1, 0, 3, 1, 0, BORDER_DEFAULT);
	imshow("webcam", image);

	gettimeofday(&t, NULL);
	double now = t.tv_sec+(t.tv_usec/1000000.0);
	
	fps++;
	if(now - last >= 1.0)
	{
		printf("FPS=%d\n", fps);
		fps = 0;
		last = now;
	}
}

int main (int argc, char *argv[])
{
	GMainLoop *loop;
	GstElement *pipeline, *source, *demuxer, *decoder, *conv, *sink;
	GstCaps *caps;
	
	namedWindow("webcam", 1);

	caps = gst_caps_new_simple("video/x-raw-rgb",
								"width", G_TYPE_INT, 320,
								"height", G_TYPE_INT, 240,
								"framerate", GST_TYPE_FRACTION, 60, 1,
								NULL);
	
	gst_init(NULL, NULL);
	loop = g_main_loop_new(NULL, FALSE);
	
	pipeline = gst_pipeline_new("webcam-pipeline");
	source   = gst_element_factory_make("v4l2src", "webcam");
	g_object_set(source, "device", argv[1], NULL);
	sink  	 = gst_element_factory_make("appsink", "asink");
	
	g_object_set(sink, "emit-signals", TRUE, NULL); 
	
	/*
	sink     = gst_element_factory_make("fakesink", "output-window");
	g_object_set(sink, "signal-handoffs", TRUE, NULL); 
	g_signal_connect(sink, "handoff", G_CALLBACK(sink_callback), NULL); 
	*/
	g_signal_connect(sink, "new-buffer", G_CALLBACK(sink_callback), NULL);
	
	if (!pipeline || !source || !sink) 
	{
		g_printerr ("One element could not be created. Exiting.\n");
		return -1;
	}
	
	gst_bin_add_many(GST_BIN(pipeline), source, sink, NULL);
	gst_element_link_filtered(source, sink, caps);
	
	/* Set the pipeline to "playing" state*/
	g_print ("Now playing: %s\n", argv[1]);
	gst_element_set_state(pipeline, GST_STATE_PLAYING);
	
	/* Iterate */
	g_print ("Running...\n");
	g_main_loop_run(loop);
	
	/* Out of the main loop, clean up nicely */
	g_print("Returned, stopping playback\n");
	gst_element_set_state(pipeline, GST_STATE_NULL);

	g_print("Deleting pipeline\n");
	gst_object_unref(GST_OBJECT(pipeline));
	
	return 0;
	
}
